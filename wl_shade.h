#if defined(USE_SHADING) && !defined(_WL_SHADE_H_)
#define _WL_SHADE_H_

#define SHADE_COUNT 32

#define LSHADE_NOSHADING 0xff
#define LSHADE_NORMAL 0
#define LSHADE_FOG 5

extern uint8_t shadetable[SHADE_COUNT][256];
#ifdef USE_ADVSHADES
extern double lightflash;
#endif 

void InitLevelShadeTable();
int GetShade(int scale
#ifdef USE_ADVSHADES
   ,int dx=0, int dy=0,int tx1=0, int ty1=0, int tx2=0,int ty2=0, byte sx=64, byte sy=64
#endif
   );
#endif
