/*  PARTICLE CODE
    BY GERARD 'ALUMIUN' WATSON
    16/12/2013
*/

#include "wl_def.h"
#include "wl_shade.h"

#ifdef NEWPARTICLES
#define RAINRATE    50
#define SNOWRATE    10
#define Z_RES       2048

extern fixed    viewx, viewy;

particle    *particlelist, *newparticle, *lastparticle;

int64_t     created, particles, destroyed;

void T_Particle(particle *ob);

statetype   p_standard              =   {false,0,0,(statefunc)T_Particle,NULL,&p_standard};

void T_Particle(particle *ob)
{
    // Move particle
    if (ob->angle != -1 && ob->speed)
    {
        ob->x += (int32_t) (ob->speed * cos(DEG2RAD(ob->angle)));
        ob->y -= (int32_t) (ob->speed * sin(DEG2RAD(ob->angle)));

        if (ob->x < 0)                  ob->x = 0;
        if (ob->x > 63 << TILESHIFT)    ob->x = 63 << TILESHIFT;
        if (ob->y < 0)                  ob->y = 0;
        if (ob->y > 63 << TILESHIFT)    ob->y = 63 << TILESHIFT;
    }

    if (ob->age)
        ob->age -= tics;
    if (ob->age < 0)
        ob->age = 0;

    ob->z += ob->zspeed;

    if (ob->z < 0)                  ob->z = 0;
    if (ob->z > Z_RES)              ob->z = Z_RES;

    ob->tilex = (short)(ob->x >> TILESHIFT);
    ob->tiley = (short)(ob->y >> TILESHIFT);

    // Check for particle death
    if (tilemap[ob->x >> TILESHIFT][ob->y >> TILESHIFT] && ob->z >= Z_RES/2)
    {
        ob->state = NULL;
        return;
    }

    switch(ob->type)
    {
        case pt_weather:
            if (ob->z <= 0 || ob->z >= Z_RES)
                ob->state = NULL;
#ifdef USE_FLOORCEILINGTEX
            else if (MAPSPOT(ob->tilex, ob->tiley, 2) >> 8)
            {
                if (ob->zspeed > 0 && ob->z >= Z_RES/2 && ob->z - ob->zspeed <= Z_RES/2)
                    ob->state = NULL;
                else if (ob->zspeed < 0 && ob->z <= Z_RES/2 && ob->z - ob->zspeed >= Z_RES/2)
                    ob->state = NULL;
            }
#endif
            break;
        case pt_timed:
            if (!ob->age)
                ob->state = NULL;
            break;
    }
}

particle *GetNewParticle(void)
{
    particle *np = new particle;
    memset(np, 0, sizeof(particle));

    if (!particlelist)
        particlelist = lastparticle = np;
    else if (lastparticle)
    {
        lastparticle->next = np;
        np->prev = lastparticle;
        lastparticle = np;
    }

    particles++;
    created++;
    return np;
}

void RemoveParticle(particle *p)
{
    if (p->prev)
        p->prev->next = p->next;
    else if (p->next)
        particlelist = p->next;

    if (p->next)
        p->next->prev = p->prev;
    else if (p->prev)
        lastparticle = p->prev;
    else
        particlelist = lastparticle = NULL;

    particles--;
    destroyed++;
    delete p;
}

void DoParticle(particle *ob)
{
    if (!ob->state)
    {
        RemoveParticle(ob);
        return;
    }

    void (*think) (particle *);

//
// non transitional object
//

    if (!ob->ticcount)
    {
        think = (void (*)(particle *)) ob->state->think;
        if (think)
        {
            think (ob);
            if (!ob->state)
            {
                RemoveParticle(ob);
                return;
            }
        }
        return;
    }

//
// transitional object
//
    ob->ticcount -= (short) tics;
    while (ob->ticcount <= 0)
    {
        think = (void (*)(particle *)) ob->state->action;        // end of state action
        if (think)
        {
            think (ob);
            if (!ob->state)
            {
                RemoveParticle(ob);
                return;
            }
        }

        ob->state = ob->state->next;

        if (!ob->state)
        {
            RemoveParticle(ob);
            return;
        }

        if (!ob->state->tictime)
        {
            ob->ticcount = 0;
            break;
        }

        ob->ticcount += ob->state->tictime;
    }

    //
    // think
    //
    think = (void (*)(particle *)) ob->state->think;
    if (think)
    {
        think (ob);
        if (!ob->state)
        {
            RemoveParticle(ob);
            return;
        }
    }
}

void DoRain(void)
{
    for (int i = 0; i < RAINRATE * tics; i++)
    {
        int x = player->x + (rand() + rand()*30) - (RAND_MAX + RAND_MAX * 30)/2;
        int y = player->y + (rand() + rand()*30) - (RAND_MAX + RAND_MAX * 30)/2;

        if (x < 0 || x > (63 << (TILESHIFT)) || y < 0 || y > (63 << (TILESHIFT)))
            continue;

        newparticle = GetNewParticle();

        newparticle->x = x;
        newparticle->y = y;
        newparticle->z = 0;
        newparticle->type = pt_weather;
        newparticle->zspeed = Z_RES/40;
        newparticle->state = &p_standard;
        newparticle->ticcount = 0;
        newparticle->angle = -1;
        newparticle->id = 0;
    }
}

void DoSnow(void)
{
    for (int i = 0; i < SNOWRATE * tics; i++)
    {
        int x = player->x + (rand() + rand()*50) - (RAND_MAX + RAND_MAX * 50)/2;
        int y = player->y + (rand() + rand()*50) - (RAND_MAX + RAND_MAX * 50)/2;

        if (x < 0 || x > (63 << (TILESHIFT)) || y < 0 || y > (63 << (TILESHIFT)))
            continue;

        newparticle = GetNewParticle();

        newparticle->x = x;
        newparticle->y = y;
        newparticle->z = 0;
        newparticle->angle = 250;
        newparticle->speed = 512;
        newparticle->type = pt_weather;
        newparticle->zspeed = Z_RES/175;
        newparticle->state = &p_standard;
        newparticle->ticcount = 0;
        newparticle->id = 1;
    }
}

void DoBlizzard(void)
{
    for (int i = 0; i < SNOWRATE * 20 * tics; i++)
    {
        int x = player->x + (rand() + rand()*50) - (RAND_MAX + RAND_MAX * 50)/2;
        int y = player->y + (rand() + rand()*50) - (RAND_MAX + RAND_MAX * 50)/2;

        if (x < 0 || x > (63 << (TILESHIFT)) || y < 0 || y > (63 << (TILESHIFT)))
            continue;

        newparticle = GetNewParticle();

        newparticle->x = x;
        newparticle->y = y;
        newparticle->z = 0;
        newparticle->type = pt_weather;
        newparticle->angle = 250;
        newparticle->speed = 2048;
        newparticle->zspeed = Z_RES/90;
        newparticle->state = &p_standard;
        newparticle->ticcount = 0;
        newparticle->id = 1;
    }
}

// An example of a particle creation function
void ExampleParticle(int32_t x, int32_t y, int32_t z)
{
    // Create a new particle
    newparticle = GetNewParticle();

    // Set the position
    newparticle->x = x;     // x position
    newparticle->y = y;     // y position
    newparticle->z = z;     // z position

    // Set the type of particle
    newparticle->state = &p_standard;   // state type (identical to object states)
    newparticle->type = pt_weather;     // behaviour
    newparticle->id = 2;                // drawing id

    // Set the particle's movement
    newparticle->speed = 2048;          // particle speed (65536 is one tile)
    newparticle->zspeed = 20;           // particle vertical speed
    newparticle->angle = 180;           // particle movement angle
    newparticle->age = 350;             // particle lifetime (70 = 1 second)

    // Other stuff
    newparticle->ticcount = 0;          // set to 1 if not using p_standard as state
}

#define RAINCOL 23
#define SNOWCOL 17

void DrawParticle(byte *vbuf, unsigned vbufPitch, int id, int vx, unsigned viewscale, particle *cur)
{
    if (vx < 0 || vx > viewwidth)
        return;

    unsigned scale = viewscale >> 3;
    if (!scale)
        return;

    int drawheight = viewheight/2 - scale*3 + (int)((cur->z/(double)Z_RES)*(scale*4));
    if (drawheight > viewheight || drawheight < 0) return;
#ifdef USE_FLOORCEILINGTEX
    if (cur->z <= Z_RES/2)
    {
        double ztotal = ABS((Z_RES/4)*3 - cur->z);
        int32_t deltax = cur->x - viewx, deltay = cur->y - viewy;
        int32_t ptx = viewx + (deltax/ztotal * (Z_RES/4)), pty = viewy + (deltay/ztotal * (Z_RES/4));
        int tilex = ptx >> TILESHIFT, tiley = pty >> TILESHIFT;

        if (MAPSPOT(tilex, tiley, 2) >> 8)
            return;
    }
#endif
    if (drawheight > viewheight/2 - (wallheight[vx] >> 3) - 2 && wallheight[vx] > viewscale) return;

#ifdef USE_SHADING
    byte *curshades = shadetable[GetShade(viewscale)];
#endif

    int length = 0;
    switch (id)
    {
        case 0:     // Rain
            length = (scale*scaleFactor)/20;
            if (length)
            {
                if (viewheight - drawheight < length) length = viewheight - drawheight;
                for (int i = 0; i < length; i++)
#ifdef USE_SHADING
                    vbuf[(drawheight + i) * vbufPitch + vx] = curshades[RAINCOL];
#else
                    vbuf[(drawheight + i) * vbufPitch + vx] = RAINCOL;
#endif
            }
            break;
        case 1:     // Snow
            length = (scale*scaleFactor)/60;
            if (length)
            {
                for (int x = 0; x < length; x++)
                    for (int y = 0; y < length; y++)
                    {
#ifdef USE_SHADING
                        if (drawheight + y < viewheight && vx + x < viewwidth)
                            vbuf[(drawheight + y) * vbufPitch + vx + x] = curshades[SNOWCOL];
#else
                        if (drawheight + y < viewheight && vx + x < viewwidth)
                            vbuf[(drawheight + y) * vbufPitch + vx + x] = SNOWCOL;
#endif
                    }
            }
            break;
    }
}

void CleanupParticles(void)
{
    if (!particlelist)
        return;

    particle *next, *p = particlelist;
    do
    {
        next = p->next;
        delete p;
    }
    while (p = next);
    particlelist = NULL;
}
#endif
