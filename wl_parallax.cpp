#include "version.h"

#ifdef USE_PARALLAX

#include "wl_def.h"

#ifdef USE_FEATUREFLAGS

// The lower left tile of every map determines the start texture of the parallax sky.
static int GetSkyTexture()
{
    int startTex = ffDataBottomLeft >> 8;
    assert(startTex >= 0 && startTex < PMSpriteStart);
    return startTex;
}
static int GetFloorTexture()
{
	int startTex = ffDataBottomLeft & 0xff;
	assert(startTex >= 0 && startTex < PMSpriteStart);
	return startTex;
}

#else

static int GetSkyTexture()
{
    int startTex;
    switch(gamestate.episode * 10 + mapon)
    {
        case  0: startTex = 20; break;
        default: startTex =  0; break;
    }
    assert(startTex >= 0 && startTex < PMSpriteStart);
    return startTex;
}

static int GetFloorTexture()
{
	int startTex;
	switch (gamestate.episode * 10 + mapon)
	{
	case  0: startTex = 10; break;
	default: startTex = 20; break;
	}
	assert(startTex >= 0 && startTex < PMSpriteStart);
	return startTex;
}
#endif

void DrawParallax(byte *vbuf, unsigned vbufPitch)
{
    int skyPage = GetSkyTexture();
    int floorPage = GetFloorTexture();
    int midangle = player->angle * (FINEANGLES / ANGLES);
    int skyheight = viewheight >> 1;
    int curtex = -1;
    byte *skytex, *floortex;

	skyPage += USE_PARALLAX - 1;
	floorPage += USE_PARALLAX - 1;

    for(int x = 0; x < viewwidth; x++)
    {
        int curang = pixelangle[x] + midangle;
        if(curang < 0) curang += FINEANGLES;
        else if(curang >= FINEANGLES) curang -= FINEANGLES;
        int xtex = curang * USE_PARALLAX * TEXTURESIZE / FINEANGLES;
        int newtex = xtex >> TEXTURESHIFT;
        if(newtex != curtex)
        {
            curtex = newtex;
            skytex = PM_GetTexture(skyPage - curtex);
            floortex = PM_GetTexture(floorPage - curtex);
        }

        int texoffs = TEXTUREMASK - ((xtex & (TEXTURESIZE - 1)) << TEXTURESHIFT);
		int yend = skyheight - (wallheight[x] >> 3);
        if(yend <= 0) continue;

		fixed u = 0;
		fixed uinc = FP(TEXTURESIZE) / skyheight;
		int y, offs;
		int botoffs = vbufPitch * (viewheight - 1) + x;
		for (y = 0, offs = x; y < yend; y++, offs += vbufPitch)
		{
			vbuf[offs] = skytex[texoffs + (u >> TILESHIFT)];

			// y-mirroring (floor)
			vbuf[botoffs] = skytex[texoffs + (u >> TILESHIFT)];
			botoffs -= vbufPitch;
			u += uinc;
		}
    }
}

#endif
